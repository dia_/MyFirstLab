package com.example.myfirstlab;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
/**
 * Класс c функциями max() и min().
 * @author Дарья Панченко.
 */
public class MainActivity extends AppCompatActivity {
    /**
     * Функция для нахождения максимального из двух элементов
     * @param a - целое (int) значение
     * @param a - целое (int) значение
     * @return возвращает максимальное число a или b
     */
    public int max(int a, int b){
        if (a >= b) {
            return a;
        }
        else
            return b;
    }

    /**
     * Функция для нахождения минимального из двух элементов
     * @param a - целое (int) значение
     * @param a - целое (int) значение
     * @return возвращает минимальное число a или b
     */
    public int min(int a, int b){
        if(a <= b) {
            return a;
        }
        else
            return b;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        MainActivity m = new MainActivity();
        m.max(4,7);
        m.min(-9,3);
    }
}
